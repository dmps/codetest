# WallStreetDocs Coding Test

## Story: As a user, I want to login and view my user profile

## Overview

This task is to create a web application in 3 hours that presents an unauthenticated landing page, with an option for the user to login via a third-party OAuth2 Identity Service, retrieve their user profile and display it in a server-rendered view, enhanced with a custom jQuery plugin.

## Deliverables

### Server-Side Tasks
- To build a simple node application that serves a single web page, and handles authentication.
- Create an express application that serves static files, and a single-page HTML file.
- Should run on http://localhost:3000 to work with the authentication service
- Create an unauthenticated view for the dashboard
- Just a simple text message and login button will suffice
- Integrate PassportJS OAuth 2 (Explicit Grant / Authentication Code flow) authentication
- Create a middleware that should be used to check whether the user is logged in or not, redirecting to the Identity Service login page, if they are not.
- Create a controller and view for the “User Profile” view
- The view should be completely rendered server-side

### Client-Side Tasks
- To display a user interface to allow the user to login and view their profile. Please construct the profile view in the most appropriate way.
- Create a basic “standard” page layout using a responsive CSS framework of your choice
- Include header and footer components
- Create a jQuery plugin to enhance the profile view


##Project Overview

- All of the templates and their css are stored in the views folder
- The main server code is in server.js
- Tests (todo) are in the spec folder
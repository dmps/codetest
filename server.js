const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const passport = require('passport');
const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;
const request = require('request')

const app = express();

app.use(express.static(path.join(__dirname, 'views')));

const exphbs = require('express-handlebars');
app.engine('handlebars',
    exphbs({
        defaultLayout: 'main'
    }));
app.set('view engine', 'handlebars');

if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
}

app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.set('port', process.env.PORT || 3000);
app.use(passport.initialize());

passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(function (user, done) {
    done(null, user);
});
passport.use('provider', new OAuth2Strategy({
        authorizationURL: '[redacted]',
        tokenURL: '[redacted]',
        clientID: '[redacted]',
        clientSecret: '[redacted]',
        callbackURL: 'http://localhost:3000'
    },
    function (accessToken, refreshToken, profile, done) {
        request.get('[redacted]', {
            'auth': {
                'bearer': accessToken
            }
        }, (err, res, body) => {
            localStorage.setItem('user', body);
            done(err, body)
        });
    }
));
// Redirect the user to the OAuth 2.0 provider for authentication.  When
// complete, the provider will redirect the user back to the application at
//     /auth/provider/callback
app.get('/auth/provider', passport.authenticate('provider'));

// The OAuth 2.0 provider has redirected the user back to the application.
// Finish the authentication process by attempting to obtain an access
// token.  If authorization was granted, the user will be logged in.
// Otherwise, authentication has failed.
app.get('/',
    passport.authenticate('provider', {
        failureRedirect: '/login',
        successRedirect: '/userprofile'
    }));

app.get('/login', function (req, res) {
    res.render('layouts/login');
})
app.get('/userprofile', function (req, res) {
    res.render('layouts/userprofile', JSON.parse(localStorage.getItem('user')))
})

app.listen(app.get('port'), function () {
    console.log('Welcome to your express instance on http://localhost:' +
        app.get('port') + '; press Ctrl-C to terminate.');
});
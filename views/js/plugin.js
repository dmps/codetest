if ( typeof Object.create !== 'function' ) {
    Object.create = function( obj ) {
        function F() {}
        F.prototype = obj;
        return new F();
    };
}
(function( $, window, document, undefined ) {
    "use strict";
    
    var TrackMouse = {

        init: function (options, elem) {
            
            var Options = {};
            
            if ( ( typeof options === 'string' ) || ( options instanceof Array ) ) {
                Options.text = options;
            } else {
                Options = options;
            }

            this.options = $.extend( {}, $.trackMouse.options, Options );

            // Start the processing, iff text provided
            if ( this.options.text ) {
                this.createTracker();
                this.bindUI();

                if ( this.options.target === false ) {
                    this.Tracker();
                };
            }
        },
        createTracker: function () {

            if ( $('.jq-mouse-tracker').length != 0 ) {
                $('.jq-mouse-tracker').remove();
            }
                
            this.el = $('<div class="jq-mouse-tracker"></div>');
            this.el.css({
                'position' : 'fixed',
                'display' : 'none',
                'left' : this.options.posX + this.options.offset.x,
                'top' : this.options.posY + this.options.offset.y,
                'z-index' : 9999999999,
                'font-family' : 'tahoma, arial, sans-serif',
                'font-size' : '14px',
                'background-color' : '#444',
                'color' : '#eee',
                'padding' : 10,
                'line-height' : '20px',
                'border-radius' : 0
            });

            $('body').append( this.el );

            if ( ( typeof this.options.text == 'string' ) || ( typeof this.options.text == 'number' ) ) {
                this.el.html( this.options.text );
            } else if ( this.options.text instanceof Array ) {

                var text = "<ul><li>" + this.options.text.join('</li><li>') + "</li></ul>";
                this.el.html( text );

                this.el.find('ul').css({
                    "margin" : 0,
                    "padding" : "5px 10px"
                });

                this.el.find('li').css({
                    "line-height" : "20px"
                });

            } else {
                this.options.text = 'Invalid text format, you can only provide string or array.';
            }

            // If tracker is to blink and target is not provided
            // ...start blinking right away.
            if ( ( this.options.blink !== false ) && ( this.options.target === false) ) {

                if ( isNaN( this.options.blink ) ) {
                    throw "`blink` must be a number or false";
                } else {
                    this.blinkTracker();
                }

            } 

            // If the tracker is meant to autohide
            // ( Allow it only if the target wasn't provided )
            if ( ( this.options.autoHide !== false ) && ( this.options.target === false ) ) {

                if ( isNaN( this.options.autoHide ) ) {
                    throw "autoHide must be a number or false";
                } else {
                    this.autoHideTracker();
                }
            }
        },

        bindUI: function () {

            var self = this;

            if ( this.options.target !== false ) {

                $(document).on('mouseover', function ( e ) {
                    if ( $( e.target ).is( $( self.options.target ) ) ) {
                        
                        self.Tracker();

                        if ( self.options.blink !== false ) {
                            self.blinkTracker();
                        };

                    } else {
                        self.hideTracker();
                        window.clearInterval( self.blink );
                    }
                });

            };

            $(document).on('mousemove', function( e ){
                $(document).trigger('trackmouse', e);
            });

            $(document).on('trackmouse', function( e, mouseObj ){
                self.moveTracker( mouseObj.clientX, mouseObj.clientY );
            });
        },




        moveTracker: function ( x, y ) {

            this.el.css({
                'left' : x + this.options.offset.x,
                'top' : y + this.options.offset.y
            });
        },

        Tracker: function () {
            this.el.show();
        },

    };
    
    $.trackMouse = function(options) {
        var trackMouse = Object.create( TrackMouse );
        trackMouse.init(options, this);

        return {
            reset : function () {
                trackMouse.reset();
            }
        }
    };

    $.trackMouse.options = {
        text : 'Provide some text',
        offset : {
            x : 10,
            y : 20
        },
        blink : false,
        autoHide : false,
        target : false
    };

    $(document).on('mousemove', function ( e ) {
        $.trackMouse.posX = e.clientX;
        $.trackMouse.posY = e.clientY;
    });

})( jQuery, window, document );